import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Buefy from "buefy";
import firebase from "firebase";
import { firestorePlugin } from "vuefire";

Vue.use(Buefy, { defaultIconPack: "fas" });

let app = "";
const firebaseConfig = {
	apiKey: "AIzaSyBB3H8E_MS5uy-v-PC2RlkWTidtHiZhWj4",
	authDomain: "nook-97517.firebaseapp.com",
	databaseURL: "https://nook-97517.firebaseio.com",
	projectId: "nook-97517",
	storageBucket: "nook-97517.appspot.com",
	messagingSenderId: "674057578897",
	appId: "1:674057578897:web:0a10ec47ae0fefda1a1230",
	measurementId: "G-BR8LR5YY43"
};
// Initialize Firebase
Vue.use(firestorePlugin);
firebase.initializeApp(firebaseConfig);

Vue.config.productionTip = false;

firebase.auth().onAuthStateChanged(() => {
	if (!app) {
		/* eslint-disable no-new */
		app = new Vue({
			router,
			render: h => h(App)
		}).$mount("#app");
	}
});

export const db = firebase.firestore();
export const auth = firebase.auth();
