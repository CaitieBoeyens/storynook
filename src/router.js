import Vue from "vue";
import Router from "vue-router";
import firebase from "firebase";

import Home from "./views/Home.vue";
import Stories from "./views/Stories.vue";
import Read from "./views/Read.vue";
import Login from "./views/Login.vue";
import Signup from "./views/Signup.vue";

Vue.use(Router);

const router = new Router({
	routes: [
		{
			path: "/",
			name: "home",
			component: Home
		},
		{
			path: "/stories",
			name: "stories",
			component: Stories,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: "/read/:name",
			name: "read",
			component: Read,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: "/login",
			name: "login",
			component: Login
		},
		{
			path: "/signup",
			name: "signup",
			component: Signup
		},
		{
			path: "/about",
			name: "about",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
				import(/* webpackChunkName: "about" */ "./views/About.vue")
		}
	]
});

router.beforeEach((to, from, next) => {
	const currentUser = firebase.auth().currentUser;
	const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

	if (requiresAuth && !currentUser) next("/");
	else next();
});

export default router;
