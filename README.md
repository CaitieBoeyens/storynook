# :rainbow: Storynook

---

## Introduction

Storynook is an app for children which aims to assist them in the process of learning to read.

Storynook employs machine learning to check the pronunciation of words. It also uses this system to read words aloud for the child to hear what they should be aiming for.

---

## Features

### Level system

Storynook has a points system which allows users to unlock more difficult content the more they read. There are 5 levels.

The stories are ranked based on the number of unique words they contain as well as the difficulty of those words.

### Pronunciation recognition

While reading the user must press the record button to record their voice, this is converted to text and compared to the text in the story.

When a user says a word correctly once it will mark it as correct in all instances on the current screen.

### Speaking aloud

Storynook also allows the users to hear a word they are unsure of so they can learn how they should be saying it.

It uses text to speech to convert the word that has been clicked into speech. It also spells out the words before reading it.

---

## Project Expansion

There is definitely room for Storynook to grow. Other areas of learning that I think AI could be helpful in digitising would be learning spelling.

I think a read aloud mode where the AI will read the whole story out loud to the user could be helpful as well. This could help children who are just starting to learn to read.

---

## Technical Details

Storynook is built using Vue for the frontend, Azure Cognitive services for the AI integration and Firebase for the backend.

Storynook is live on [storynook.netlify.com](https://storynook.netlify.com).

A video demo can be viewed [here](https://drive.google.com/file/d/1Fw-6FOzVYtzLsDEL0pE8Pz0l_sbfiPOr/view?usp=sharing).

The stories on Storynook where sourced from [freekidsbooks.org/](https://freekidsbooks.org/).

All icons are from [Font Awesome](https://fontawesome.com/icons).

### Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```
